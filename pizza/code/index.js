'use strict'

const pizza = {
    size: "85"
};

const price = {
    size: {
        small: 50,
        mid: 75,
        big: 85
    },
    sauce: {
        sauceClassic: 10,
        sauceBBQ: 12,
        sauceRikotta: 12,
    },
    topping: {
        moc1: 10,
        moc2: 15,
        moc3: 12,
        telya: 15,
        vetch1: 10,
        vetch2: 15
    }
};

window.addEventListener("DOMContentLoaded", () => {
    document.getElementById("pizza")
    .addEventListener("click", function (ev) {
        switch (ev.target.id) {
            case "small": pizza.size = price.size.small;
                break
            case "mid": pizza.size = price.size.mid;
                break
            case "big": pizza.size = price.size.big;
                break  
        }
        show(pizza);
    });

    const draggableItems = document.querySelectorAll('.draggable');
    draggableItems.forEach(item => {
        item.addEventListener('dragstart', event => {
            event.dataTransfer.setData('id', item.id);
            event.dataTransfer.effectAllowed = "copy";
        });
    });

    const table = document.querySelector('.table');
    table.addEventListener('dragover', event => {
        event.preventDefault();
    });

    table.addEventListener('drop', event => {
        event.preventDefault();
        const itemId = event.dataTransfer.getData('id');
        const draggedItem = document.getElementById(itemId).cloneNode(true);
        table.append(draggedItem);

        switch (event) {
            case itemId: pizza.size = price.sauce.sauceClassic;
                break
            case itemId: pizza.size = price.sauce.sauceBBQ;
                break
            case itemId: pizza.size = price.sauce.sauceRikotta;
                break
            case itemId: pizza.size = price.topping.moc1;
                break
            case itemId: pizza.size = price.topping.moc2;
                break
            case itemId: pizza.size = price.topping.moc3;
                break
            case itemId: pizza.size = price.topping.telya;
                break
            case itemId: pizza.size = price.topping.vetch1;
                break
            case itemId: pizza.size = price.topping.vetch2;
                break
        }
    show(pizza);
    });

    show(pizza);
    btnRun();
});

function show(pizza) {
    const priceElement = document.querySelector("#price");
    priceElement.innerText = `${(Object.values(pizza).reduce((allSum, a) => Number(allSum + a), 0))} грн.`;
}

function btnRun () {
    const btn = document.querySelector("#banner");
    btn.addEventListener("mousemove", () => {
        const coords = {
            X : Math.floor(Math.random() * document.body.clientWidth),
            Y : Math.floor(Math.random() * document.body.clientHeight)
        }
        console.log(coords.X - 300);
        console.log(document.body.clientWidth);
        if((coords.X + 350) > document.body.clientWidth){
            return
        }
        if((coords.Y + 150) > document.body.clientHeight){
            return
        }
        btn.style.top = coords.Y + "px";
        btn.style.left = coords.X + "px";
    })
}

const nameInput = document.querySelector('input[name="name"]');
const phoneInput = document.querySelector('input[name="phone"]');
const emailInput = document.querySelector('input[name="email"]');
const submitButton = document.getElementById('btnSubmit');

const nameRegex = /^[а-яА-ЯёЁa-zA-Z\s']+$/;
const phoneRegex = /^\+380\d{9}$/;
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

function validateInput(input, regex) {
    return regex.test(input.value);
}

submitButton.addEventListener('click', event => {
    event.preventDefault();
    if (!validateInput(nameInput, nameRegex)) {
        alert('Введіть коректне ім\'я');
        return;
    }
    if (!validateInput(phoneInput, phoneRegex)) {
        alert('Введіть коректний номер телефону у форматі +380XXXXXXXXX');
        return;
    }
    if (!validateInput(emailInput, emailRegex)) {
        alert('Введіть коректну адресу електронної пошти');
        return;
    }
        document.location.href = './thank-you/index.html';
});